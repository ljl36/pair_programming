package szys;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class szysTest {

	@Test
	void testGenerateExps() {
		System.out.println("testGenerateExps");
		ArrayList<ExpAndAns> exSet = Arithmetic.generateExps(5);
		for (ExpAndAns expAndAns : exSet) {
			System.out.println(expAndAns.getExp() + "=" + expAndAns.getAns());
		}
		System.out.println();
	}

	@Test
	void testGenerateNewExp() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		System.out.println("testGenerateNewExp");
		Class<Arithmetic> clazz = Arithmetic.class;
		Method generateNewExp = clazz.getDeclaredMethod("generateNewExp");// jdk新特性：可变参数
		generateNewExp.setAccessible(true);// 设置该方法可见
		System.out.println(generateNewExp.invoke(clazz));
		System.out.println();
	}

	@Test
	void testTrans() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		System.out.println("testTrans");
		Class<Arithmetic> clazz = Arithmetic.class;
		Method trans = clazz.getDeclaredMethod("trans", String.class);// jdk新特性：可变参数
		trans.setAccessible(true);// 设置该方法可见
		System.out.println("(-12)-(-14)^0-(18-(-10))" + "->" + trans.invoke(clazz, "(-12)-(-14)^0-(18-(-10))"));
		System.out.println("7-(8)+(-3)-(-10)" + "->" + trans.invoke(clazz, "17-(8)+(-3)-(-10)"));
		System.out.println("13*7/0^0" + "->" + trans.invoke(clazz, "13*7/0^0"));
		System.out.println();
	}

	@Test
	void testCalc() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		System.out.println("testCalc");
		Class<Arithmetic> clazz = Arithmetic.class;
		Method calc = clazz.getDeclaredMethod("calc", String.class);
		calc.setAccessible(true);
		Method trans = clazz.getDeclaredMethod("trans", String.class);
		trans.setAccessible(true);
		System.out.println("19*7*6-4+(-16)" + "=" + calc.invoke(clazz, trans.invoke(clazz, "19*7*6-4+(-16)")));
		System.out.println("(-11)+(-8)/((-16)+(-10))" + "=" + calc.invoke(clazz, trans.invoke(clazz, "(-11)+(-8)/((-16)+(-10))")));
		System.out.println("3-(-5)-(-7)" + "=" + calc.invoke(clazz, trans.invoke(clazz, "3-(-5)-(-7)")));
		System.out.println("5-(-9)*(-14)*(12)^0" + "=" + calc.invoke(clazz, trans.invoke(clazz, "5-(-9)*(-14)*(12)^0")));
		System.out.println();
	}

	@Test
	void testMinPre() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		System.out.println("testMinPre");
		Class<Arithmetic> clazz = Arithmetic.class;
		Method minPre = clazz.getDeclaredMethod("minPre", Node.class);
		minPre.setAccessible(true);
		Method trans = clazz.getDeclaredMethod("trans", String.class);
		trans.setAccessible(true);
		Method createExpTree = clazz.getDeclaredMethod("createExpTree", String.class);
		createExpTree.setAccessible(true);
		String exp1 = "(256+3*57)*60-18";
		String exp2 = "60*(57*3+256)-18";
		String postexp1 = (String) trans.invoke(clazz, exp1);
		String postexp2 = (String) trans.invoke(clazz, exp2);
		Node root1 = (Node) createExpTree.invoke(clazz, postexp1);
		Node root2 = (Node) createExpTree.invoke(clazz, postexp2);
		MinPreFormat minPreFormat1 = (MinPreFormat) minPre.invoke(clazz, root1);
		MinPreFormat minPreFormat2 = (MinPreFormat) minPre.invoke(clazz, root2);
		System.out.println(minPreFormat1.getVal());
		System.out.println(minPreFormat2.getVal());
		System.out.println(exp1 + " " + exp2 + ":" + (minPreFormat1.equals(minPreFormat2) ? "same" : "different"));
		exp2 = "(256+3*57)*(60-18)";
		postexp2 = (String) trans.invoke(clazz, exp2);
		root2 = (Node) createExpTree.invoke(clazz, postexp2);
		minPreFormat2 = (MinPreFormat) minPre.invoke(clazz, root2);
		System.out.println(minPreFormat2.getVal());
		System.out.println(exp1 + " " + exp2 + ":" + (minPreFormat1.equals(minPreFormat2) ? "same" : "different"));
	}

}
